GitAutoVersion — README
########################

GitAutoVersion generates a version string for the current working directory in a git repository in the format:

    ``<major>.<minor>.<patch>[+<upcount>][!<draft>][.<dirty>][#<branch>.<commit>]``

GitAutoVersion depends on version tags in the format of `v<major>.<minor>.<patch>`.
It uses the output of `git describe` will reformate the suffixes accordingly.
If the current checked out commit has a valid version tag the script will use the version tag as the current version, skipping the suffixes.

Installation
************

Installation through the Python Package Index (`PyPi <https://pypi.org/>`_) is recommended:

.. code-block:: python

    pip --install GitAutoVersion

Usage
*****

This package provides the funtion `getVersionString` which returns the generated version string for the working direcory of the given repository.
It requires a git repository object of the GitPython module as argument.

.. code-block:: python

    version = GitAutoVersion.getVersionString(git_repo)

You can modify the text for the special flags:

``flag_development:str = "DEVELOPMENT"``
    Indicating that the version is in a development branch.

``flag_git:str = "git"``
    Indicating that the metadata-chunk is a git Sha-1 hash. Set to `none` to omit the prefix.

``flag_branch:str = "branch"``
    How do you call a "branch"? Set to `none` to omit the prefix.

``flag_dirty:str = "DIRTY"``
    What to say if the working directory is dirty


Caveats
*******

This project is under development. It may break at any time.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.